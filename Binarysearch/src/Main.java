import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;


public class Main {
	
	public static void main(String[] args) {
		int[] arr = new int[100];
		//Random rnd = new Random();
		for(int i = 0; i < arr.length; i++)
		{
			arr[i] = i;
		}
		arr = insertionSort(arr);
		int zahl;
		Scanner scan = new Scanner(System.in);
		boolean weiter = true;
		do {
			System.out.println("Geben sie die zu suchende Zahl an:\n");
			
			//try {
				zahl = scan.nextInt();
				int pos = binarySearch(zahl, arr);
				System.out.println("Die finale position" + pos);
				
			//} catch (Exception e) {
				System.out.println("Eingabe nicht gueltig. Geben Sie bitte eine Zahl ein"
						+ " z.B.(1, 40, -100");
				//System.out.println(e.getMessage());
			//}
			
			
			
		} while (weiter);
	}
	
	public static int binarySearch(int zahl, int[] arr)
	{
		int laenge = arr.length;
		int pos = (laenge/2);
		int quozient = 1;
		int tiefe = 0;
		pos = binarySearch(zahl, arr, pos, quozient, tiefe);
		return pos;
		
	}
	
	public static int binarySearch(int zahl, int[] arr, int pos, int quozient, int tiefe)
	{
		tiefe++;
		
		if(zahl == arr[pos])
		{
			
			System.out.println("gefunden mit " + tiefe + " durchlaeufen!");
			return pos;

		}
		if(((pos/2) < 1) || (pos == (-1)) || (pos >= arr.length) || (zahl < arr[0]))
		{
			System.out.println("nicht vorhanden, " + tiefe + " durchlaeufe");
			return -1;
			
		}
		if(zahl < arr[pos])
		{
			pos = (pos / 2);
			System.out.println("kleiner, position: " + pos);
			pos = binarySearch(zahl, arr, pos, quozient, tiefe);
			if(pos == (-1))
			{
				return -1;
			}
		}
		if(zahl > arr[pos])
		{
			System.out.println("groesser, postion: " + pos);
			quozient = quozient * 2;
			
			if(((pos/quozient) != 0) && (quozient != 0) && ((pos + pos /quozient) < arr.length))
			{
				pos = pos + pos / quozient;
			}else if(pos < arr.length)
			{
				pos = pos +1 ;
			}else
			{
				return -1;
			}
			pos = binarySearch(zahl, arr, pos, quozient, tiefe);
			if(pos == (-1))
			{
				return -1;
			}
		}
		return pos;
		
	}
	
	public static int[] insertionSort(int x[])
	{
		ArrayList<Integer> aL = new ArrayList<Integer>();
		aL.add(x[0]);
		int index = 0;
		
		for(int i = 1; i < (x.length); i ++)
		{
			int z = x[i];
			for(int a: aL)
			{
				
				if(z > a) 
				{
					index ++;
				}
			}
			aL.add(index, z);
			index = 0;
			
		}
		int[] arr = new int[x.length];
		int i = 0;
		for(int a: aL)
		{
			
			arr[i] = a;
			i++;
		}
		
		return arr;
	}

}
