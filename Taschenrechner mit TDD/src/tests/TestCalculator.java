package tests;


import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import mainPackage.Calculate;


public class TestCalculator {
	


	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPlus() {
		assertEquals(10, Calculate.plus(4.3, 5.7), 0.0001);
	}
	
	@Test
	public void testminus()
	{
		assertEquals(-3.0, Calculate.minus(7.0, 10.0), 0.0001);
	}
	
	@Test
	public void testTimes()
	{
		assertEquals(20, Calculate.times(5, 4), 0.0001);
	}
	
	@Test
	public void testDivided()
	{
		assertEquals(1.0, Calculate.divided(10, 10), 0.0001);
	}
	
	@Test
	public void testQuadrat()
	{
		assertEquals(25, Calculate.quadrat(5), 0.0001);
	}

	@Test
	public void testLog()
	{
		assertEquals(0, Calculate.logarythmus(1), 0.0001);
	}
}
