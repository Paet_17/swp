package mainPackage;

public class BuchstabezuZahl implements CryptoStrategy{
	
	public BuchstabezuZahl(){
		
		
	}
	
	@Override
	public String ecrypt(String s) {
		s = s.toUpperCase();
		char [] txtChar = s.toCharArray();
		String [] ecryptTxt = new String [txtChar.length];
		String verschluesselt = "";
		ConfigManager cm = new ConfigManager();
		
		for (int i = 0; i < txtChar.length; i++) {
			String c = Character.toString(txtChar[i]);
			String ch = cm.getPropertyM(c);
			if (!(ch == null)) {
				ecryptTxt[i] = ch;
			}else{
				ecryptTxt[i] = "00";
			}
			
		}
		for (String string : ecryptTxt) {
			verschluesselt += string;
		}
		
		return verschluesselt;
	}

	@Override
	public String decrypt(String s) {
		char [] txtChar = s.toCharArray();
		String [] decTxt = new String[txtChar.length];
		String entschlluesselt = "";
		ConfigManager cm = new ConfigManager();
		String c = "";
		int y = 0;
		for(int i = 0; i < txtChar.length; i++){
			
			c += Character.toString(txtChar[i]);
			if(c.length() == 1) continue;
			String ch;
			
			if(c.equals("00")){
				ch = " ";
				
			}else{
				ch = cm.getPropertyM(c);
				
			}
			c="";
			
			decTxt[y] = ch;
			y++;
			
		}
		
		for (String string : decTxt) {
			if(string == null) break;
			entschlluesselt += string;
		}
		
		
		return entschlluesselt;
	}

}
