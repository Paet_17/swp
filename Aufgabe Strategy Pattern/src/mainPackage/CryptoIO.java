package mainPackage;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CryptoIO {
	
	private String text;
	BufferedReader br = null;
	BufferedWriter bw = null;
	CryptoStrategy cStrategy;
	Basticrypto man = Basticrypto.getInstance();
	
    
	
	public CryptoIO(String path){
		read(path, false);
	}
	
	void write(String fPath, boolean ecrypt){
		
		if (ecrypt) {
			text = cStrategy.ecrypt(this.text);
		}
		try{
			bw = new BufferedWriter( new FileWriter(fPath));
		    bw.write(text);
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}finally{
			try {
				bw.close();
			} catch (IOException e){
				e.printStackTrace();
			}
		}
		
	}
	
	void read(String fPath, boolean decrypt){
		
			String line;
			text = "";
			
			try{
				
				br = new BufferedReader(new FileReader(fPath));
				while ((line = br.readLine()) != null){
					
						text += line + "\n";
					
					
				}
			}catch(FileNotFoundException e){
				e.printStackTrace();
			}catch (IOException e) {
	            
	        } finally {
	            if (br != null) {
	                try {
	                    br.close();
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	            }
	        }
			
			if (decrypt) {
				text = cStrategy.decrypt(text);
			}
	}
	
	String getText(){
		return this.text;
	}
	void setText(String text){
		this.text = text;
	}
	
	void setCryptoStrategy(CryptoStrategy strat){
		this.cStrategy = strat;
		if( strat instanceof Basticrypto){
			man.init(man.password.toCharArray(), man.salt, man.iterations);
		}
	}

}
