package mainPackage;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;


public class ConfigManager {
	

	Properties prop = new Properties();
	OutputStream output = null;
	InputStream input = null;
	
	public ConfigManager()
	{
		
	}

	public String getPropertyM(String name) {
		
		String key = name;
		
		try{
			input = new FileInputStream("zuordnung.properties");
			prop.load(input);
			name = prop.getProperty(key);
		}catch (IOException ex)
		{
			ex.printStackTrace();
		}finally
		{
			if(input != null){
				try{
					input.close();
				}
					catch(IOException e)
					{
						e.printStackTrace();
					}
				}
			}
		return name;
	}

	

}
