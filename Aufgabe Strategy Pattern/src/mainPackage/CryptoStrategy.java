package mainPackage;

public interface CryptoStrategy {
	
	String ecrypt(String s);
	
	String decrypt(String s);

}
