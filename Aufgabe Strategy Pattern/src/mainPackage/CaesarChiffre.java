package mainPackage;

public class CaesarChiffre implements CryptoStrategy{
	
	private static int key = 17;
	

	@Override
	public String ecrypt(String s) {
		char [] txtChar = s.toCharArray();
		char [] ecryptTxt = new char [txtChar.length];
		String verschluesselt = "";
		
		for(int i = 0; i< txtChar.length; i++){
			
			int verschiebe = (txtChar[i] + key)%128;
			
			ecryptTxt[i] = (char) (verschiebe);
			
		}
		
		
		for (char c : ecryptTxt) {
			verschluesselt += c;
		}
		
		
		return verschluesselt;
	}

	@Override
	public String decrypt(String s) {
		
		char [] txtCrypted = s.toCharArray();
		char [] decrTxt = new char[txtCrypted.length];
		
		String entschluesselt = "";
		int verschiebe;
		
		for (int i =0; i < txtCrypted.length; i++){
			if (txtCrypted[i] - key < 0) verschiebe = txtCrypted[i] - key + 128;
			else verschiebe = (txtCrypted[i] - key)%128;
			
			decrTxt[i] = (char) (verschiebe);
		}
		for (char c : decrTxt) {
			entschluesselt += c;
		}
		return entschluesselt;
	}
	
	

}
