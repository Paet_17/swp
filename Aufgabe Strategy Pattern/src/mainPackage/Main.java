package mainPackage;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		CryptoIO ci = new CryptoIO("text.txt");
		ArrayList<CryptoStrategy> csL = new ArrayList<CryptoStrategy>();
		csL.add(new CaesarChiffre());
		csL.add(new BuchstabezuZahl());
		csL.add(new Basticrypto());
	
		for (CryptoStrategy cryptoStrategy : csL) {
			ci.setCryptoStrategy(cryptoStrategy);
			System.out.println("Methode: " + cryptoStrategy.getClass().getSimpleName());
		    
			System.out.println("Gelesen: ");
			ci.read("text.txt", false);
			System.out.println(ci.getText());
			
			ci.write("text.txt", true);
			System.out.println("Geschrieben und verschluesselt:" + ci.getText());
			
			ci.read("text.txt", true);
			System.out.println("Entschluesselt, gelesen: " + ci.getText());
			
			ci.setText(cryptoStrategy.decrypt(ci.getText()));
			ci.write("text.txt",false);
		}
		
		
		
		/*
		//Test .write method
		
		ci.write("text.txt");
		System.out.println("Gelesener Text: \n" + ci.getText());
			
		//Test .read method
		ci.read("text.txt");
		System.out.println("Geschriebener encrypteter Text: \n" + ci.getText());
		
		//Test .write method
		ci.read("text.txt");
		ci.write("text.txt");
		System.out.println("Gelesener Text (decrypted): \n" + ci.getText());
		
		
		
		/*Test configManager
		ConfigManager cm = new ConfigManager();
		System.out.println(cm.getPropertyM("A"));
	*/
		
		
		
		/*Test .read method
		CryptoIO ci = new CryptoIO();
		ci.read("text.txt");
		System.out.println(ci.getText());
		
		//Test .write method
		
		String s = ci.getText();
		s += "\nDieser Text wurde ins File geschrieben!";
		ci.setText(s);
		ci.write("text.txt");
		System.out.println(ci.getText());
		*/
		
		//Teste verschluesseln
		/*
		CaesarChiffre cC = new CaesarChiffre();
		String verschluesselt = cC.ecrypt(ci.getText());
		System.out.println("Verschluesselt: \n" + verschluesselt);
		
		//Test entstschluesseln
		String etschl = cC.decrypt(verschluesselt);
		System.out.println("Entschluesselt: \n" + etschl);
		*/
	}

}
