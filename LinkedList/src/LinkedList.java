
public class LinkedList {
	Node head;
	Node tail;
	
	public LinkedList()
	{
		head = null;
		tail = null;
	}
	
	public void add(int i)
	{
		Node n = new Node(i);
		
		if(head == null)
		{
			head = n;
		}
		else
		{
			Node nLauf;
			nLauf = head;
			while(nLauf.hasNext())
			{
				nLauf = nLauf.getNext();
			}
			nLauf.setNext(n);
			n.setPrevious(nLauf);
			tail = n;
			
		}
	}
	
	public int size()
	{
		int i = 0;
		Node nlauf = head;
		while(nlauf != null)
		{
			nlauf = nlauf.getNext();
			i++;
		}
		nlauf = tail;
		return i;
	}
		

	public void print()
	{
		Node nlauf = head;
		do
		{
			System.out.println(nlauf.getValue());
			nlauf = nlauf.getNext();
		}
		while(nlauf != null);
		nlauf = tail;
		
	}
	
	public void sort()
	{
				for (int i = 0; i < 9; i++) {
					Node a = head;
					Node b = a.getNext();
					Node previous = null;
						for (int j = 0; j < size()-i; j++) {
							if (a.getValue() > b.getValue()) {
								swap(a, b, previous);
								
								if (a.hasNext()) {
									b = a.getNext();
									
								}
								else
								{
									break;
								}
							} else {
								previous=a;
								a = a.getNext();
								if(!a.hasNext())
								{
									break;
								}
								b = a.getNext();
							}
							
						}
				}
	}

	private void swap(Node a, Node b, Node previous) {
		a.setNext(b.getNext());
		b.setNext(a);

		if (a == head) {
			head = b;
		}else{
			previous.setNext(b);
		}
	}
}