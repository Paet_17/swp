
public class DoubleLinkedList {

	public class Node {
		private Node next;
		private Node previous;

		public Node getPrevious() {
			return previous;
		}

		public void setPrevious(Node previous) {
			this.previous = previous;
		}

		private int i;

		public Node(Node next, Node previous, int i) {
			this.next = next;
			this.previous = previous;
			this.i = i;
		}

		public Node getNext() {
			return next;
		}

		public void setNext(Node next) {
			this.next = next;
		}

		public int getValue() {
			return i;
		}

		public void setI(int i) {
			this.i = i;
		}

	}

	private Node head = null;

	public void printList() {
		Node n = head;
		while (n.next != null) {
			System.out.println(n.getValue());
			n = n.getNext();
		}
		System.out.println(n.getValue());
	}

	public void addElement(int i) {

		if (head == null) {
			head = new Node(null, null, i);
		} else {
			Node n = head;
			while (n.next != null) {
				n = n.getNext();
			}
			n.setNext(new Node(null, n, i));
		}

	}

	public int size() {
		int size = 0;
		Node n = head;
		while (n != null) {
			size++;
			n = n.getNext();
		}
		return size;
	}

	public void addElement(int i, int pos) {

		if (pos == 0) {
			Node t = new Node(head, null, i);
			head = t;
		} else {
			Node n = head;
			for (int j = 0; j < (pos - 1); j++) {
				n = n.getNext();
			}
			Node temp = n.getNext();
			n.setNext(new Node(null, n, i));
			n = n.getNext();
			n.setNext(temp);
		}

	}

	public void Sort() {

		for (int i = 0; i < size(); i++) {
			Node m=head;
			for (int j = 0; j < size() - i; j++) {
				if (m.getValue() > m.getNext().getValue()) {
					Node smaller = m.getNext();
					swap(m, smaller);

					if (head == m) {
						head = smaller;
					}
					
					if(m.getNext()==null)
						break;

				} else {
					if(m.getNext().getNext()!=null)
					m = m.getNext();

				}
			}

		}

	}

	private void swap(Node m, Node smaller) {
		if (m.getPrevious() != null)
			m.getPrevious().setNext(smaller);
		smaller.setPrevious(m.getPrevious());
		if (smaller.getNext() != null)
			smaller.getNext().setPrevious(m);
		m.setPrevious(smaller);
		m.setNext(smaller.getNext());
		smaller.setNext(m);
	}

}
