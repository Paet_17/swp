public class Node{
	
	Node next;
	Node previous;
	int value;
		
		public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

		public Node(int val){
			this.value = val;
			next = null;
			previous = null;
		}
		
		public Node getPrevious() {
			return previous;
		}

		public void setPrevious(Node previous) {
			this.previous = previous;
		}

		public void setNext(Node n)
		{
			this.next = n;
		}
		
		public boolean hasNext(){
			if(this.next == null){
				return false;
			}
			return true;
		}
		
		public boolean hasPrevious()
		{
			if(this.previous == null)
			{
				return false;
			}
			return true;
		}

		public Node getNext() {
			return next;
		}
		
		
		
}