package sqliteInterface;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sqlite.SQLiteConfig;


public class DBManager {
	
	String dbName;
	String tbName1;
	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public String getTbName1() {
		return tbName1;
	}

	public void setTbName1(String tbName1) {
		this.tbName1 = tbName1;
	}

	public String getTbName2() {
		return tbName2;
	}

	public void setTbName2(String tbName2) {
		this.tbName2 = tbName2;
	}



	String tbName2;
	
	String t1c1;
    String t1c2;
	String t1c3;
	String t2c1;
	String t2c2;
	String t2c3;
	public String getT2c3() {
		return t2c3;
	}

	public void setT2c3(String t2c3) {
		this.t2c3 = t2c3;
	}

	public String getT2c4() {
		return t2c4;
	}

	public void setT2c4(String t2c4) {
		this.t2c4 = t2c4;
	}



	String t2c4;
	
	Connection c = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	String comm = "";
	
	public DBManager() throws SQLException
	{
		ConfigManager cm = new ConfigManager();
		dbName = cm.getDbName();
		tbName1 = cm.getTbName1();
		tbName2 = cm.getTbName2();
		
		t1c1 = cm.getT1C1();
		t1c2 = cm.getT1C2();
		t1c3 = cm.getT1C3();
		t2c1 = cm.getT2C1();
		t2c2 = cm.getT2C2();
		t2c3 = cm.getT2C3();
		t2c4 = cm.getT2C4();
		OpenConnection();
		System.out.println("First connection to zug.db was successfull!");
		c.close();
		createDatabase();
	}
	
	public String getT1c2() {
		return t1c2;
	}

	public void setT1c2(String t1c2) {
		this.t1c2 = t1c2;
	}

	public String getT1c3() {
		return t1c3;
	}

	public void setT1c3(String t1c3) {
		this.t1c3 = t1c3;
	}

	public String getT2c2() {
		return t2c2;
	}

	public void setT2c2(String t2c2) {
		this.t2c2 = t2c2;
	}

	public void OpenConnection() throws SQLException 
	{
		
		try{
			Class.forName("org.sqlite.JDBC");
			SQLiteConfig config = new SQLiteConfig();  
	        config.enforceForeignKeys(true);
	        c=DriverManager.getConnection("jdbc:sqlite:" +dbName,config.toProperties());
			c.setAutoCommit(false);
			
			
		}
		catch (Exception e)
		{
			System.err.println(e.getClass().getName() + ": " + e.getMessage() );
			System.out.println("Verbindung zur Datenbank ist nicht moeglich!");
			System.exit(0);
		}
	}

	public void createDatabase() throws SQLException
	{
		OpenConnection();
		
	
		
		comm = "create table if not exists " + tbName1 + "("
				+ t1c1 + " integer primary key,"
				+ t1c2 + " text not null,"
				+ t1c3 + " integer not null);";
		ps = c.prepareStatement(comm);
		ps.executeUpdate();
		comm = "create table if not exists " + tbName2 + " ( "
				+ t2c1 + " integer primary key,"
				+ t2c2 + " integer not null, "
				+ t2c3 + " integer not null, "
				+ t2c4 + " integer not null,"
				+ "FOREIGN KEY( "  + t2c4 + " ) REFERENCES " + tbName1 + "( " + t1c1 + ")"
								+ ");";
		ps = c.prepareStatement(comm);
		ps.executeUpdate();
		c.commit();
		ps.close();
		c.close();
		
	}

	//IF the table has 3 columns 
	public void insert(String table, int val1, int val2, String sVal1) throws SQLException
	{
		OpenConnection();
		
		ps = c.prepareStatement("INSERT INTO " + table +
				"(" + t1c1 + "," + t1c2 + "," + t1c3 + " )VALUES(?,?,?)");
	      ps.setInt(1, val1);
	      ps.setString(2, sVal1);
	      ps.setInts(3, val2);
	      ps.executeUpdate();
	      ps.close();
	      c.commit();
	      c.close();
	}
	
	//IF the table has 4 columns
	public void insert(String table, int val1, int val2, int val3, int val4) throws SQLException
	{
		OpenConnection();
		PreparedStatement ps = c.prepareStatement("INSERT INTO " + table +
				"(" + t2c1 +"," + t2c2 +", " + t2c3 + ", " + t2c4 + ")VALUES(?,?,?,?)");
	      ps.setInt(1, val1);
	      ps.setInt(2, val2);
	      ps.setInt(3, val3);
	      ps.setInt(4, val4);
	      ps.executeUpdate();
	      ps.close();
	      c.commit();
	      c.close();
	}

	
	
	
	public boolean loeschen(String tab, int zeile, String idName) throws SQLException
	{
		OpenConnection();
		String comm = "DELETE FROM " + tab + " WHERE " + idName +  "=" + zeile;
		PreparedStatement ps = c.prepareStatement(comm);
		ps.executeUpdate();
		c.commit();
		ps.close();
		c.close();
		return true;
		
	}

	public String getT1c1() {
		return t1c1;
	}

	public void setT1c1(String t1c1) {
		this.t1c1 = t1c1;
	}

	public String getT2c1() {
		return t2c1;
	}

	public void setT2c1(String t2c1) {
		this.t2c1 = t2c1;
	}

	public void select() throws SQLException
	{
		OpenConnection();
		comm ="SELECT * FROM " + tbName1 + " join " + tbName2 + " using( " + t1c1 + " );" ;
		ps = c.prepareStatement(comm);
		rs= ps.executeQuery();
		System.out.println("\n Topic_id \t name \t RFID \t Time_id \t start \t end");
			 
			 while(rs.next())
			 {
				System.out.print("\n" + rs.getInt(t1c1) + "\t");
				System.out.print(rs.getString(t1c2) + "\t");
				System.out.print(rs.getInt(t1c3) + "\t");
				System.out.print(rs.getInt(t2c1) + "\t");
				System.out.print(rs.getInt(t2c2) + "\t");
				System.out.print(rs.getInt(t2c3) + "\t \n");
				
			 }
		ps.close();
		rs.close();
		c.close();
	}

}
