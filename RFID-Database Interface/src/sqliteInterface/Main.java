package sqliteInterface;

import java.sql.SQLException;
import java.util.Scanner;

public class Main {
	
	static String chosen;
	static Scanner scan = new Scanner(System.in);
	static DBManager dm;
	static boolean go = true;
	
	static String tbName1;
	static String tbName2;

	public static void main(String[] args) throws SQLException {
		
		dm = new DBManager();
		tbName1 = dm.getTbName1();
		tbName2 = dm.getTbName2();
		
		
		
		while(go)
		{
			menue();
		}
		

	}
	
	public static boolean menue() throws SQLException
	{
		int val1;
		int val2;
		int val3;
		int val4;
		String sVal1;
		
		System.out.println("Hello and welcome to the programe acting as a interface to your database! \n");
		System.out.println("Please choose an action::");
		System.out.println("Add " + tbName1 + "\t ... \t o");
		System.out.println("Add " + tbName2 + "\t ... \t i");
		System.out.println("Select \t ... \t s");
		System.out.println("Delete entry \t ... \t d");
		System.out.println("Update entry \t ... \t u");
		System.out.println("Type any other letter/number to exit.");
		
		chosen = scan.next();
		switch(chosen){
		case "o":
			System.out.println("Which id has the new " + tbName1 + " ?");
			val1 = scan.nextInt();
			System.out.println("Which name has the new topic?");
			sVal1 = scan.next();
			System.out.println("Which rfid number has the topic?");
			val2 = scan.nextInt();
			dm.insert(tbName1, val1, val2, sVal1 );
			break;
		case "i":
			System.out.println("Which id has the new time?");
			val1 = scan.nextInt();
			System.out.println("What is the starttime??");
			val2 = scan.nextInt();
			System.out.println("What is the endTime??");
			val3 = scan.nextInt();
			System.out.println("To which topic does the time belong?");
			val4 = scan.nextInt();
			dm.insert(tbName2, val1, val2, val3, val4);
			break;
		case "s":
			dm.select();
			break;
			
		case "d":
			System.out.println();
			String tab = deleteMenue();
			System.out.println("Which one do you want to delete?(ID)");
			int zeile = scan.nextInt();
			String idName = "";
			if(tab.equals(tbName1)) idName = dm.getT1c1();
			else if(tab.equals(tbName2)) idName = dm.getT2c1();
			if(dm.loeschen(tab, zeile, idName))
			{
				System.out.println("Deleted successfully!");
			}
			else
			{
				System.out.println("A Proplem occured!");
			}
			break;
		case "u":
			break;
			
		default:
			System.out.println("Good bye!");
			go = false;
			return go;
			
		}
		return true;
		
		
	}
	
	public static String deleteMenue()
	{
		String s;
		System.out.println("From which table do you want to delete?");
		System.out.println("People ... p \nBooking ... b \nWagon ... w");
		s = scan.next();
		switch(s){
		case"p":
			s = tbName1;
			break;
		case"b":
			s=tbName2;
			break;
		}
		return s;
	}
	
}

