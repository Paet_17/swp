package sqliteInterface;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;


public class ConfigManager {
	
	private String dbName = "rfid.db";
	private String tbName1 = "topic";
	private String tbName2 = "time";
	private String t1c1 = "topic_ID";
	private String t1c2 = "name";
	private String t1c3 = "rfid";
	private String t2c1 = "time_ID";
	private String t2c2 = "startTime";
	private String t2c3 = "endTime";
	private String t2c4 = t1c1;
	
	Properties prop = new Properties();
	OutputStream output = null;
	InputStream input = null;
	
	public ConfigManager()
	{
		try {
			output = new FileOutputStream("config.properties");
			
			prop.setProperty("dbName", dbName);
			prop.setProperty("tbName1", tbName1);
			prop.setProperty("tbName2", tbName2);
			prop.setProperty("t1c1", t1c1);
			prop.setProperty("t1c2", t1c2);
			prop.setProperty("t1c3", t1c3);
			prop.setProperty("t2c1", t2c1);
			prop.setProperty("t2c2", t2c2);
			prop.setProperty("t2c3", t2c3);
			prop.setProperty("t2c4", t2c4);
			
			prop.store(output, null);
		} catch (IOException io) {
			io.printStackTrace();
		} finally{
			if (output != null){
				try{
					output.close();
				} catch (IOException e){
					e.printStackTrace();
				}
			}
		}
	}

	
	public String getDbName()
	{
		String dbName = "";
		dbName = getPropertyM("dbName");
		
		return dbName;
		}


	private String getPropertyM(String name) {
		
		String key = name;
		
		try{
			input = new FileInputStream("config.properties");
			prop.load(input);
			name = prop.getProperty(key);
		}catch (IOException ex)
		{
			ex.printStackTrace();
		}finally
		{
			if(input != null){
				try{
					input.close();
				}
					catch(IOException e)
					{
						e.printStackTrace();
					}
				}
			}
		return name;
	}
	
	public String getTbName1()
	{
		String tbName1 = "";
		tbName1 = getPropertyM("tbName1");
		
		return tbName1;
	}

	public String getTbName2()
	{
		String tbName2 = "";
		tbName2 = getPropertyM("tbName2");
		
		return tbName2;
	}
	
	
	public String getT1C1()
	{
		return (getPropertyM("t1c1"));
	}
	
	public String getT1C2()
	{
		return (getPropertyM("t1c2"));
	}
	
	public String getT1C3()
	{
		return (getPropertyM("t1c3"));
	}
	
	public String getT2C1()
	{
		return (getPropertyM("t2c1"));
	}
	
	public String getT2C2()
	{
		return (getPropertyM("t2c2"));
	}
	
	public String getT2C3()
	{
		return (getPropertyM("t2c3"));
	}
	
	public String getT2C4()
	{
		return (getPropertyM("t2c4"));
	}
	
	

}
