import java.util.ArrayList;




public class Main {

	public static void main(String[] args) {
		
		int[] haufenUnordnung = new int[10];
		System.out.println("Ein Haufen von Unordnung: ");
		for(int i = 0; i <= 9; i++ )
		{
			haufenUnordnung[i] = (int) (Math.random() * 100);
			System.out.println(haufenUnordnung[i]);
		}
		
		System.out.println("\n\nEs wurde ein Haufen Ordnung mit Bubblesort!");
		int[] haufenOrdnung = bubbleSort(haufenUnordnung);
		for(int i = 0; i <= 9; i++ )
		{
			System.out.println(haufenOrdnung[i]);
		}
		
		System.out.println("\n\nHaufen von Ordnung mit insertionSort");
		
		ArrayList<Integer> aL = insertionSort(haufenUnordnung);
		for(int a:aL)
		{
			System.out.println(a);
		}
		
		

	}
	
	/*
	public static int[] sortArray(int[] x)
	{
		int[] zuSortieren = x;
		
		for(int i = 0; i <= 9; i++)
		{
			for(int j = i + 1; j<= 9; i++)
			{
				zuSortieren
			}
		}
	}
	*/
	
	public static int[] bubbleSort(int[] u)
	{
		int[] arr = u;
		for(int i=0; i < (u.length-1); i++	)
		{
			for(int j = 0; j < (u.length-i-1); j++)
			{
				if(arr[j] > arr[j+1])
				{
					swap(arr, j, (j+1));
				}
			}
		}
		
		return u;
	}
	
	public static ArrayList<Integer> insertionSort(int x[])
	{
		ArrayList<Integer> aL = new ArrayList<Integer>();
		aL.add(x[0]);
		int index = 0;
		
		for(int i = 1; i < (x.length); i ++)
		{
			int z = x[i];
			for(int a: aL)
			{
				
				if(z > a) 
				{
					index ++;
				}
			}
			aL.add(index, z);
			index = 0;
			
		}
		
		return aL;
	}
	
	public static int[] swap(int[] x, int z, int z2)
	{
		int w = x[z];
		x[z] = x[z2];
		x[z2] = w;
		return x;
	}

}
