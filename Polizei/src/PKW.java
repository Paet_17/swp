
public class PKW extends Fahrzeug{

	private String besonderheiten;
	
	
	
	public PKW(int baujahr, String hersteller, String farbe, String typ, String besonderheiten) {
		super(baujahr, hersteller, farbe, typ);
		
		this.besonderheiten = besonderheiten;
	}



	public String getBesonderheiten() {
		return besonderheiten;
	}



	public void setBesonderheiten(String besonderheiten) {
		this.besonderheiten = besonderheiten;
	}
	
	

}
