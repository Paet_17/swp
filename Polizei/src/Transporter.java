
public class Transporter extends Fahrzeug{
	
	
	private double ladeflaeche;
	private int sitzplaetze;
	
	public Transporter(int baujahr, String hersteller, String farbe, String typ, double ladeflaeche, int sitzplaetze) {
		super(baujahr, hersteller, farbe, typ);
		this.ladeflaeche = ladeflaeche;
		this.sitzplaetze = sitzplaetze;
	}
	
	
	
	public double getLadeflaeche() {
		return ladeflaeche;
	}
	public void setLadeflaeche(double ladeflaeche) {
		this.ladeflaeche = ladeflaeche;
	}
	public int getSitzplaetze() {
		return sitzplaetze;
	}
	public void setSitzplaetze(int sitzplaetze) {
		this.sitzplaetze = sitzplaetze;
	}

}
