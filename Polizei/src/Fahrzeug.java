
public class Fahrzeug {
	
	private int baujahr;
	private String hersteller;
	private String farbe;
	private String typ;
	
	public Fahrzeug(int baujahr, String hersteller, String farbe, String typ)
	{
		this.baujahr = baujahr;
		this.hersteller = hersteller;
		this.farbe = farbe;
		this.typ = typ;
	}

	public int getBaujahr() {
		return baujahr;
	}

	public void setBaujahr(int baujahr) {
		this.baujahr = baujahr;
	}

	public String getHersteller() {
		return hersteller;
	}

	public void setHersteller(String hersteller) {
		this.hersteller = hersteller;
	}

	public String getFarbe() {
		return farbe;
	}

	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

}
