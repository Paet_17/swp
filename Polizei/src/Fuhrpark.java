import java.util.ArrayList;


public class Fuhrpark {
	
	private ArrayList<PKW> pkwL = new ArrayList<PKW>();
	private ArrayList<Motorrad> motorL = new ArrayList<Motorrad>();
	private ArrayList<Transporter> transL = new ArrayList<Transporter>();
	
	public Fuhrpark()
	{
		PKW p1 = new PKW(1998, "Audi", "geld", "PKW", "subwoofer");
		Motorrad m1 = new Motorrad(2003, "Yavasaki", "schwarz", "Motorrad", 123);
		Transporter t1 = new Transporter(1990, "Mann", "blau", "Transporter", 123.45, 5);
		
		pkwL.add(p1);
		motorL.add(m1);
		transL.add(t1);
		
	}
	
	public Fahrzeug ausleihen(String typ)
	{
		Fahrzeug f = null;
		if(typ.equals("PKW"))
		{
			if(pkwL.size() != 0) return null;
			f = (PKW)pkwL.get(0);
			pkwL.remove(0);
		}
		if(typ.equals("Motorrad"))
		{
			if(pkwL.size() != 0) return null;
			f = (Motorrad)motorL.get(0);
			pkwL.remove(0);
		}
		if(typ.equals("Transporter"))
		{
			if(pkwL.size() != 0) return null;
			f = (Transporter)transL.get(0);
			pkwL.remove(0);
		}
		return f;
	}
	
	public boolean zurr�ckgeben(Fahrzeug f)
	{
		if(f.getTyp().equals("PKW"))
		{
			pkwL.add((PKW) f);
		}
		if(f.getTyp().equals("Motorrad"))
		{
			motorL.add((Motorrad) f);
		}
		if(f.getTyp().equals(("Transporter")))
		{
			transL.add((Transporter) f );
		}
		return true;
	}

}
