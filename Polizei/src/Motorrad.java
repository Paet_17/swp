
public class Motorrad extends Fahrzeug{

	private int hubraum;
	
	public Motorrad(int baujahr, String hersteller, String farbe, String typ, int hubraum) {
		super(baujahr, hersteller, farbe, typ);
		this.hubraum = hubraum;
	}

	public int getHubraum() {
		return hubraum;
	}

	public void setHubraum(int hubraum) {
		this.hubraum = hubraum;
	}

}
