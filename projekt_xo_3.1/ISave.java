import java.io.Serializable;
import java.util.ArrayList;


@SuppressWarnings("serial")
public class ISave implements Serializable{
	
	ArrayList<Aufgabe> aL = new ArrayList<Aufgabe>();
	ArrayList<Projekt> pL = new ArrayList<Projekt>();
	
	public ISave(ArrayList<Projekt> pL, ArrayList<Aufgabe> aL) {
		super();
		this.aL = aL;
		this.pL = pL;
	}

	public ArrayList<Aufgabe> getaL() {
		return aL;
	}

	public void setaL(ArrayList<Aufgabe> aL) {
		this.aL = aL;
	}

	public ArrayList<Projekt> getpL() {
		return pL;
	}

	public void setpL(ArrayList<Projekt> pL) {
		this.pL = pL;
	}
	
	

}
