import java.io.Serializable;

import com.sun.xml.internal.bind.v2.runtime.RuntimeUtil.ToStringAdapter;

@SuppressWarnings("serial")
public class Aufgabe implements Serializable{

	private int zeitGeplant;
	private int zeitReal;
	private int zeitDiff;
	private String aufgabeName;
	private String aufgabeErklaerung;
	private int id;
	
	public Aufgabe(int id, int zeitGeplant, int zeitReal, String aufgabeName, String aufgabeErklaerung) {
		super();
		this.setId(id);
		this.zeitGeplant = zeitGeplant;
		this.zeitReal = zeitReal;
		this.aufgabeName = aufgabeName;
		this.aufgabeErklaerung = aufgabeErklaerung;
	}

	public int getZeitGeplant() {
		return zeitGeplant;
	}

	public void setZeitGeplant(int zeitGeplant) {
		this.zeitGeplant = zeitGeplant;
	}

	public int getZeitReal() {
		return zeitReal;
	}

	public void setZeitReal(int zeitReal) {
		this.zeitReal = zeitReal;
	}

	public String getAufgabeName() {
		return aufgabeName;
	}

	public void setAufgabeName(String aufgabeName) {
		this.aufgabeName = aufgabeName;
	}

	public String getAufgabeErklaerung() {
		return aufgabeErklaerung;
	}

	public void setAufgabeErklaerung(String aufgabeErklaerung) {
		this.aufgabeErklaerung = aufgabeErklaerung;
	}
	
	public int getZeitDiff()
	{
		zeitDiff = zeitGeplant - zeitReal;
		return zeitDiff;
	}
	
	public void setZeitDiff(int zeitDiff)
	{
		this.zeitDiff = zeitDiff;
	}
	
	public String toString()
	{
		return aufgabeName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	
}
